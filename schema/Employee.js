import mongoose from "mongoose";

const employeeSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: String,
    profession: String,
    color : String, 
    city : String, 
    branch : String, 
    createdAt: Date,
    updatedAt: Date,

},
{
    collection: 'employees',
    timestamps: true
});

var EmployeeModel = mongoose.model("Employee", employeeSchema);

module.exports = EmployeeModel;