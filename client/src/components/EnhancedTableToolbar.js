import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';
import { lighten } from '@material-ui/core/styles/colorManipulator';
import { Input } from '@material-ui/core';
import Modelscreen from './model';

export default class EnhancedTableToolbar extends React.Component {

    constructor(props){
        super(props);
        this.onDelete = this.onDelete.bind(this);
    }

    componentDidUpdate(){
        console.log('Selected  = ', this.props.selectedIds)
    }
    onDelete() {
        const ids = this.props.selectedIds;
        const onDeleted = this.props.onDeleted;

        var url = 'http://localhost:6002/employees/delete';
        console.log(url);
        fetch(url, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ ids })
        }).then(res => res.text())
            .then(res => {
                // alert(res);
                onDeleted();
            })
    }

    render() {
        const { numSelected, classes, selectedIds, onDeleted } = this.props;

        return (
            <Toolbar
                className={classNames(classes.root, {
                    [classes.highlight]: numSelected > 0,
                })}
            >
                <div className={classes.title}>
                    {numSelected > 0 ? (
                        <Typography color="inherit" variant="subtitle1">
                            {numSelected} selected
                </Typography>
                    ) : (
                            <Typography variant="h6" id="tableTitle">
                                Employees
                </Typography>
                        )}
                </div>
                <div className={classes.spacer} style={{ justifyContent: 'center', marginRight: 400, }
    }/>
                <div  className={classes.actions}>
                    {numSelected > 0 ? (
                        <div  >
                            <div >
                                <Tooltip title="Delete">
                                    <IconButton aria-label="Delete">
                                        <DeleteIcon onClick={this.onDelete}  />
                                    </IconButton>
                                </Tooltip>
                            </div>
                            {selectedIds.length === 1 && <div className={classes.actions} >
                                <Tooltip className={classes.actions} style={{ width: 40, }} title="Edit">
                                    <IconButton aria-label="Edit">
                                        <Modelscreen 
                                           id={selectedIds[0]} 
                                           refreshEmployeeList={() =>{
                                            onDeleted()
                                           }} 
                                         />
                                    </IconButton>
                                </Tooltip>
                            </div>}
                        </div>


                    ) : (
                            <Tooltip title="Filter list">
                                <IconButton aria-label="Filter list">
                                    <FilterListIcon />
                                </IconButton>
                            </Tooltip>
                        )}
                </div>
            </Toolbar>
        );

    }
};

EnhancedTableToolbar.propTypes = {
    classes: PropTypes.object.isRequired,
    numSelected: PropTypes.number.isRequired,
    selectedIds: PropTypes.array,
    onDeleted: PropTypes.func
};

EnhancedTableToolbar = withStyles(toolbarStyles)(EnhancedTableToolbar);

const toolbarStyles = theme => ({

    highlight:
        theme.palette.type === 'light'
            ? {
                color: theme.palette.secondary.main,
                backgroundColor: lighten(theme.palette.secondary.light, 0.85),
            }
            : {
                color: theme.palette.text.primary,
                backgroundColor: theme.palette.secondary.dark,
            },
    spacer: {
        flex: '1 1 100%',
    },
    actions: {
        color: theme.palette.text.secondary,
    },
    title: {
        flex: '0 0 auto',
    },
});