import React from 'react';
import Modal from 'react-responsive-modal';
import Paper from '@material-ui/core/Paper';
import { Input } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import { API_URL } from '../config/variables';

export default class Model extends React.Component {
  state = {
    open: false,
    _id: '',
    name: '',
    profession: '',
    color: '',
    city: '',
    branch: '',
  };

  async componentDidMount() {
    console.log('The id', this.props.id);
    let id = this.props.id;
    await this.getEmployee(id);
  }

  async getEmployee(id) {
    let url = API_URL + '/employees/getOne?id='+id;
    let apiCall = await fetch(url);
    let employee = await apiCall.json();

    await this.setState({ ...employee });
  }

  saveEmployee = async (e) => {
    e.preventDefault();

    const { name, branch, city, profession, color, _id } = this.state;

    let url = API_URL + '/employees/update';
    let apiCall = await fetch(url, { 
      method: 'POST', 
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ 
        employee: {
          name, branch, city, profession, color, _id
        }
      })
    });

    let response = await apiCall.json();

    // refresh list
    await this.props.refreshEmployeeList();

    // close model
    return await this.setState({ open: false });
    
  }



  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };

  render() {
    const { open } = this.state;
    return (
      <div style={{ width: 350 }}>
        <EditIcon onClick={this.onOpenModal}> Open modal</EditIcon>
        <Modal open={open} onClose={this.onCloseModal} >
          <>
            <Paper style={{ marginBottom: 90, width: 250, }}>
              <form onSubmit={this.saveEmployee} style={{ marginTop: '50%', padding: '10%' }}>
                <div style={{ marginBottom: 20, marginLeft: 15, color: 'black', fontSize: 19, fontWeight: 500, }}>Edit Employee</div>
                <label style={{ padding: '10%' }}>Name</label>
                <div style={{ top: 100 }}>
                  <Input onChange={event => this.setState({ name: event.target.value })} value={this.state.name} type="text" name="email" component="input" style={{ marginLeft: "10%" }} />
                </div>
                <label style={{ padding: '10%' }}>Profession</label>
                <div>
                  <Input onChange={event => this.setState({ profession: event.target.value })} value={this.state.profession} id="profession" type="test" component="input" style={{ marginLeft: "10%" }} />
                </div>
                <label style={{ padding: '10%' }}>Color</label>
                <div>
                  <Input onChange={event => this.setState({ color: event.target.value })} value={this.state.color} id="color" type="test" component="input" style={{ marginLeft: "10%" }} />
                </div>
                <label style={{ padding: '10%' }}>City</label>
                <div>
                  <Input onChange={event => this.setState({ city: event.target.value })} value={this.state.city} id="city" type="test" component="input" style={{ marginLeft: "10%" }} />
                </div>
                <label style={{ padding: '10%' }}>Branch</label>
                <div>
                  <Input onChange={event => this.setState({ branch: event.target.value })} value={this.state.branch} id="branch" type="text" component="input" style={{ marginLeft: "10%" }} />
                </div>
                <button type="submit" style={{ padding: 10, marginLeft: '17%', marginTop: '20%', marginBottom: 10, borderRadius: 10, color: 'blue', background: 'red', fontSize: 'medium' }}>Save</button>
              </form>
            </Paper>
          </>
        </Modal>
      </div>
    );
  }
}

