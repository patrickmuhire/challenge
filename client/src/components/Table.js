import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';
import { lighten } from '@material-ui/core/styles/colorManipulator';
import { Input } from '@material-ui/core';
import logo from '../assets/plex.png';
// import addemployess from '../../schema/Employee'
import EnhancedTableToolbar from './EnhancedTableToolbar';
import EnhancedTableHead from './EnhancedTableHead';



let counter = 0;
function createData(name, calories, fat, carbs, protein) {
  counter += 1;
  return { id: counter, name, calories, fat, carbs, protein };
}

function desc(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function stableSort(array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
  return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}


const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 1,
  },
  table: {
    minWidth: 10,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
});

class EnhancedTable extends React.Component {
  state = {
    order: 'asc',
    orderBy: 'calories',
    selected: [],
    data: [],
    page: 0,
    name: '',
    profession: '',
    color: '',
    city: '',
    branch: '',
    rowsPerPage: 5,
    open: false,
  };

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    this.setState({ order, orderBy });
  };

  handleSelectAllClick = event => {
    if (event.target.checked) {
      this.setState(state => ({ selected: state.data.map(n => n._id) }));
      return;
    }
    this.setState({ selected: [] });
  };

  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    this.setState({ selected: newSelected });
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  async componentDidMount() {
    await this.listEmployees();
  }

  async listEmployees() {
    var that = this;
    var url = 'http://localhost:6002/employees/list';
    try {
      let apiCall = await fetch(url);
      const data = await apiCall.json();
      await this.setState({ data: data.docs });
      console.log(data);
      return;
    } catch (error) {
      console.log('Error', error);
      return;

    }
  }

  onPost = async (e) => {
    e.preventDefault();  
    var url = 'http://localhost:6002/employees/add';

    let apiCall =  await fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        name: this.state.name,
        profession: this.state.profession,
        color: this.state.color,
        city: this.state.city,
        branch: this.state.branch,
      }),
    });

    await this.setState({
        name: '',
        profession: '',
        color: '',
        city: '',
        branch: '',
    })

    await this.listEmployees();
  }

  render() {
    const { classes } = this.props;
    const { data, order, orderBy, selected, rowsPerPage, page } = this.state;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);

    return (

      <div >
        <div style={{ marginLeft: 70, top: 50, }}>
          <img  style={{width: 300}}src={logo} alt="logo" />
        </div>

        <div >
        </div>
        <div >
          <div style={{ marginLeft: 130, width: '23%', marginBottom: 20, }}>
            <Paper className={classes.root}>
              <form onSubmit={this.onPost} style={{ padding: '10%' }}>
                <div style={{ marginBottom: 20, marginLeft: 15, color: 'black', fontSize: 19, fontWeight: 500, }}>Add Employee</div>
                <label style={{ padding: '10%' }}>Name</label>
                <div style={{ top: 100 }}>
                  <Input onChange={event => this.setState({ name: event.target.value })} value={this.state.name} type="text" name="email" component="input" style={{ marginLeft: "10%" }} />
                </div>
                <label style={{ padding: '10%' }}>Profession</label>
                <div>
                  <Input onChange={event => this.setState({ profession: event.target.value })} value={this.state.profession} id="profession" type="test" component="input" style={{ marginLeft: "10%" }} />
                </div>
                <label style={{ padding: '10%' }}>Color</label>
                <div>
                  <Input onChange={event => this.setState({ color: event.target.value })} value={this.state.color} id="color" type="test" component="input" style={{ marginLeft: "10%" }} />
                </div>
                <label style={{ padding: '10%' }}>City</label>
                <div>
                  <Input onChange={event => this.setState({ city: event.target.value })} value={this.state.city} id="city" type="test" component="input" style={{ marginLeft: "10%" }} />
                </div>
                <label style={{ padding: '10%' }}>Branch</label>
                <div>
                  <Input onChange={event => this.setState({ branch: event.target.value })} value={this.state.branch} id="branch" type="text" component="input" style={{ marginLeft: "10%" }} />
                </div>
                <button type="submit" style={{ padding: 10, marginLeft: '17%', marginTop: '20%', marginBottom: 10, borderRadius: 10, color: 'blue', background: 'red', fontSize: 'medium' }}>Add Employee</button>
              </form>
            </Paper>
          </div>
        </div>
        < div style={{
          height: '50%',
          width: '55%',
          marginTop: -470,
          marginLeft: 450,

        }}>
          <Paper className={classes.root}>
            <EnhancedTableToolbar 
               numSelected={selected.length} 
               selectedIds={selected}
               onDeleted={async () => {
                   await this.listEmployees();
               }}
            />
            <div className={classes.tableWrapper}>
              <Table className={classes.table} aria-labelledby="tableTitle">
                <EnhancedTableHead
                  numSelected={selected.length}
                  order={order}
                  orderBy={orderBy}
                  onSelectAllClick={this.handleSelectAllClick}
                  onRequestSort={this.handleRequestSort}
                  rowCount={data.length}
                />
                <TableBody>
                  {stableSort(data, getSorting(order, orderBy))
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map(n => {
                      const isSelected = this.isSelected(n._id);
                      return (
                        <TableRow
                          hover
                          onClick={event => this.handleClick(event, n._id)}
                          role="checkbox"
                          aria-checked={isSelected}
                          tabIndex={-1}
                          key={n._id}
                          selected={isSelected}
                        >
                          <TableCell>
                            <Checkbox checked={isSelected} />
                          </TableCell>
                          <TableCell component="th" scope="row" >
                            {n._id}
                          </TableCell>
                          {/* <TableCell align="center">{n._id}</TableCell> */}
                          <TableCell align="center">{n.name}</TableCell>
                          <TableCell align="center">{n.profession}</TableCell>
                          <TableCell align="ceter">{n.color}</TableCell>
                          <TableCell align="center">{n.city}</TableCell>
                          <TableCell align="center">{n.branch}</TableCell>
                        </TableRow>
                      );
                    })}
                  {emptyRows > 0 && (
                    <TableRow style={{ height: 10 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
              </Table>
            </div>
            <TablePagination
              rowsPerPageOptions={[5, 10, 25]}
              component="div"
              count={data.length}
              rowsPerPage={rowsPerPage}
              page={page}
              backIconButtonProps={{
                'aria-label': 'Previous Page',
              }}
              nextIconButtonProps={{
                'aria-label': 'Next Page',
              }}
              onChangePage={this.handleChangePage}
              onChangeRowsPerPage={this.handleChangeRowsPerPage}
            />
          </Paper>
        </ div>
      </div>
    );
  }

}

EnhancedTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(EnhancedTable);