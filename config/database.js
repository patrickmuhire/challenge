import mongoose from "mongoose";

mongoose.connect(
  "mongodb+srv://vugadev:oPG3Cqerr9TXgOJC@cluster0-jez7h.mongodb.net/employee?retryWrites=true&authSource=employee&ssl=true",
    { 
      useNewUrlParser: true
    }
  );

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  // we're connected!
  console.log("Connected to MongoDB database")
});

module.exports = db;