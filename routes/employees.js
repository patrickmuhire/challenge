import express from "express";
import mongoose from "mongoose";
import Employee from "../schema/Employee";

const router = express.Router();

router.get("/", (req, res, next) => {
    res.render("employee");   
});

router.get("/hoes", (req, res, next) => {
    res.status(200).json({
        message:"Suck my dick"
    });   
});

router.get("/getOne", async (req, res, next) => {
    const id = req.query.id;
    let employee = await Employee.findById(id).exec();
    return res.json(employee);
});

router.get("/list", async (req, res, next) => {
    let employees = await Employee.find({}).exec();
    return res.json({ docs: employees });
});

router.post("/update", async (req, res, next) => {
    const bodyEmployer = req.body.employee;

    await Employee.update({ _id: bodyEmployer._id }, bodyEmployer).exec();
    return res.json({ updated: true });

});

router.post("/add", async (req, res, next) => {

    const employee = new Employee({
        _id: mongoose.Types.ObjectId(),
        name: req.body.name,
        profession:req.body.profession,
        color: req.body.color,
        city: req.body.city,
        branch: req.body.branch, 
    });

    await employee.save();

    return res.json({ docs:[employee] });
    
});

router.post("/delete", async (req, res, next) => {
    const selectedIds = req.body.ids;

    if(selectedIds.length === 0){
        return res.json({ empty: 'blah blah'})
    };

    await Promise.all(selectedIds.map(async (id, index) =>{
        let employee = await Employee.findById(id).exec()
        employee.remove();
        return;
    }));

    return res.json({ deleted: true });

    /***
     
    const rid = req.body.id;

    Employee.findById(rid)
        .exec()
        .then(docs => {
            docs.remove();
            res.status(200).json({
                deleted:true
            });
        })
        .catch(err => {
            console.log(err)
        });
     * 
     */
    
});




module.exports = router;